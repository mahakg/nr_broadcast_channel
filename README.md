insertFakeMailRecords.php ---> This file will insert fake mails in table mail_queue. for now, if this file is executed it'll insert 10000 records, they can be changed by changing its value.

broadcastMessages.php ---> This file will broadcast the messages depending on the configuration specied in config file 

config.php ---> File to keep configuration changes

MailQueueService.php ---> Service to process a Mail Queue. This will run as a daemon or a cronjob in a definite period of time so that recieving mail servers don't declare the outgoing IP as a SPAM address.

mailCounter.php ---> View file to show real time interaction of mailqueue

js/ --- JS libraries

queueCounter.php ---> API to return the status of pending emails and emails with error.

Database.php -----> File for connecting to Database. Please change your mysql database name and password

BroadcastChannel.php ---> Interface which will act as a container for all the broadcast mediums. If any new Broadcast Medium is added, It'll i
mplement this Interface.


mail_files -----> SMTP, POP3 Protocol Files to send mail


To Send Emails - It is required to set up a postfix server pointing to a remote domain.