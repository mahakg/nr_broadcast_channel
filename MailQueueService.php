<?php
include 'BroadcastChannel.php';
include 'mail_files/PHPMailer.php';
include 'mail_files/class.smtp.php';
include 'mail_files/class.pop3.php';
include 'Database.php';
class MailQueueService implements BroadcastChannel
{
    private $db;
    public function __construct(){
        $this->db = Database::getInstance();
    }

        public function run()
        {
            $mail = new PHPMailer;

            $mail->IsSMTP();
            $mail->Host  = "localhost";
            $mail->SMTPAuth = "";

            // Batch of 30 Emails will be processed at a time
            $sql = "SELECT * from mail_queue where error='n' LIMIT 0,30";
            $query = $this->db->query($sql);

            $queue=array();
            $i=0;
            while($mail_row=mysql_fetch_assoc($query))
            {
                $queue[$i] = $mail_row;
                $i++;
            }

            foreach($queue as $key=>$row) {
                $mail->ClearAddresses();
                $mail->ClearCCs();
                $mail->ClearBCCs();
                $mail->IsHTML(true);
                $mail->CharSet = 'UTF-8';
                $mail->SetFrom($row['senderemail'],$row['sendername']);
                $mail->Subject    = $row['subject'];
                $mail->AddAddress($row['recpemail'], "");
                $mail->AltBody = wordwrap(strip_tags($row['message']));
                $mail->MsgHTML(wordwrap($row['message']));

                // Get attachments if any
                if ($row['attachments'] != "") {
                        $attacharray = explode("%%_n_%%",$row->attachments);
                        for ($i=0;$i<sizeof($attacharray);$i++) {
                        $mail->AddAttachment("$attacharray[$i]",basename($attacharray[$i]));
                        }
                }


                if($row['cc'] !=  '')
                {
                        $cc_addrs = explode(',', $row['cc']);
                        foreach($cc_addrs as $cc_addr)
                        {
                                $mail->AddCC($cc_addr, "");
                        }
                }
                if($row['bcc'] !=  '')
                {
                        $bcc_addrs = explode(',', $row['bcc']);
                        foreach($bcc_addrs as $bcc_addr)
                        {
                                $mail->AddBCC($bcc_addr, "");
                        }
                }

                if(!$mail->Send()) {
                  echo "Mailer Error: " . $mail->ErrorInfo;
                  $error_update_sql="UPDATE mail_queue SET error='y' where id='$row[id]'";
                  $this->db->query($error_update_sql);
                }
                else {
                        $delete_sql="DELETE from mail_queue where id='$row[id]'";
                        $this->db->query($delete_sql);
                        exit;
                }
            }

            echo "Process completed at " .date("Y-m-d H:i:s")."\n";
            sleep(5);

        }


}
