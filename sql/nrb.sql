create database newrubric_broadcast;

CREATE TABLE `mail_queue` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `senderemail` varchar(255) DEFAULT NULL,
 `sendername` varchar(255) DEFAULT NULL,
 `recpemail` varchar(255) DEFAULT NULL,
 `subject` varchar(255) DEFAULT NULL,
 `message` text,
 `error` enum('y','n') DEFAULT NULL,
 `createdon` datetime DEFAULT NULL,
 `updatedon` datetime DEFAULT NULL,
 `attachments` varchar(2048) DEFAULT NULL,
 `cc` varchar(255) DEFAULT NULL,
 `bcc` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;
