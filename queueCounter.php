<?php
include 'Database.php';

$db = Database::getInstance();

$queue_sql="SELECT COUNT(*) from mail_queue where error='n'";
$mail_pending_count = $db->query($queue_sql);

$queue_sql="SELECT COUNT(*) from mail_queue where error='y'";
$mail_error_count= $db->query($queue_sql);


$mail_count= $db->getRow($mail_pending_count);
$error_count = $db->getRow($mail_error_count);
header('Content-Type: application/json');
echo json_encode(array('mail_count'=>$mail_count[0], 'error_count'=>$error_count[0]));
?>