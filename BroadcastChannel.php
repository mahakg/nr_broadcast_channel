<?php
/**
 * Created by PhpStorm.
 * User: mahak
 * Date: 20/6/14
 * Time: 12:33 AM
 */

// All communication channels will implement this Interface which will define function run
interface BroadcastChannel {

    public function run();

} 