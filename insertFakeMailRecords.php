<?php
include 'Database.php';

$num_records = 10000;

$db = Database::getInstance();

function generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

for($i=1; $i<$num_records; $i++){
	$senderMail = generateRandomString();
	$senderMail .= '@sample.com';
	$recpeMail = generateRandomString();
	$recpeMail .= '@sample.com';
	$cc = generateRandomString();
	$cc .= '@sample.com';
	$bcc = generateRandomString();
	$bcc .= '@sample.com';
	$subject = generateRandomString(14);
	$message = generateRandomString(110);
	$error = 'n';
	$sendername = generateRandomString();
	$sql = "INSERT into mail_queue(senderemail, sendername, recpemail, subject, message, error, createdon, cc, bcc)
		VALUES ('$senderMail', '$sendername', '$recpeMail', '$subject', '$message', '$error', now(), '$cc', '$bcc')";
	#$query= mysql_query($sql);
	$query= $db->query($sql);
	if($query){
		echo 'Database Updated';
	}
}
?>
