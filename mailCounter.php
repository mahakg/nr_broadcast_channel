<html>
<head>
	<script src="js/jquery.min.js"></script>
	<script>
        $(document).ready(function(){

            function counter(){
                $.ajax({
                    url: "queueCounter.php",
                    method: 'get',
                    type: 'json',
                    success: function (data) {
                        $("#count1").html(data.mail_count);
                        $("#count2").html(data.error_count);
                    }
                })}
            counter();
            setInterval( function(){counter()}, 3000);
        });

	</script>
</head>
<body >
<div style="margin: auto; width:50%; background-color:grey">
<span><h3>Real time processing of Broadcast Queue - This page will keep on refreshing the count in every 5 seconds</h3></span>
<div id="div1">Emails under process (to be sent) - <span id="count1"></span></div>
<div id="div2">Emails still in queue (updated with error) - <span id="count2"></span></div>
</div>
</body>
</html>